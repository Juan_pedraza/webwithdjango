from django.http import HttpResponse
import datetime
from django.template import Template, Context, loader
from django.shortcuts import render

def saludo(request):

    framework = "Django n__n"
    # doc_externo = open("myWebSite/plantillas/plantilla.html")
    # plantilla = Template(doc_externo.read())
    # doc_externo.close()
    # contexto = Context({"framework": framework})
    # doc_externo = loader.get_template('plantilla.html')
    # documento = doc_externo.render({"framework": framework})
    # return HttpResponse(documento)
    return render(request,"plantilla.html", {"framework": framework})

def fecha(req):
    fecha_actual = datetime.datetime.now() 

    return HttpResponse(f"<h3>La hora y fecha actual es: {fecha_actual}</h3>")

def CalcularEdad(req,age,year):

    tiempo = year-2021
    edadFutura = age+tiempo

    return HttpResponse(f'En el año {year}, tendrás {edadFutura} años')
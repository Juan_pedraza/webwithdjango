from django.contrib import admin
from Pedidos.models import Clients,Articles,Pedidos

# Register your models here.

class ClientesAdmin(admin.ModelAdmin):
    list_display=("name","adress","email","tel")
    # search_fields = ()



admin.site.register(Clients,ClientesAdmin)
admin.site.register(Articles)
admin.site.register(Pedidos)
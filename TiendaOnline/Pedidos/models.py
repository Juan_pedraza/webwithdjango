from django.db import models

# Create your models here.

# Entidad Clientes
class Clients(models.Model):
    name = models.CharField(max_length=30)
    adress = models.CharField(max_length=45)
    # email = models.EmailField(blank=True, null=True) Si queremos que este campo no sea requerido
    email = models.EmailField()   
    tel = models.CharField(max_length=10)

    def __str__(self):
        return self.name

# Entidad Artículos
class Articles(models.Model):
    name = models.CharField(max_length=30)
    section = models.CharField(max_length=20)
    prize = models.IntegerField()

    # def __str__(self):
    #     name = self.name
    #     section = self.section
    #     prize = self.prize
        
    #     return "El artículo {name} de la sección {section} tiene un valor de {prize}"
    # Si queremos que nos devuelva un string con la sentencia select

# Entidad Pedidos
class Pedidos(models.Model):
    number = models.IntegerField()
    date = models.DateField()
    entregado = models.BooleanField()